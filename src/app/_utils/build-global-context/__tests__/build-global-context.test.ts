import '@testing-library/jest-dom'
import { buildGlobalContext } from '../build-global-context';

describe('buildGlobalContext', () => {
  it('builds global context with no search params', async() => {
    expect(buildGlobalContext({})).toEqual({
      networkBaseUrl: 'https://www.minds.com'
    });
  });

  it('builds global context with search params', async() => {
    const baseUrl: string = 'https://www.minds.com';
    const actionColor: string = '#fff000';

    expect(buildGlobalContext({baseUrl: baseUrl, actionColor: actionColor})).toEqual({
      networkBaseUrl: baseUrl,
      actionColor: actionColor
    });
  });
});