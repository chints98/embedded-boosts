import  { GlobalContextType } from '../../_types/global-context';
import { SearchParams } from '../../_types/search-params';

/** Default base URL. */
const DEFAULT_BASE_URL: string = 'https://www.minds.com';

/**
 * Build global context object from search params.
 * @param { SearchParams } searchParams - Search params. 
 * @returns { GlobalContextType } - Build global context object from search params.
 */
export const buildGlobalContext = (searchParams: SearchParams): GlobalContextType => {
  if (!searchParams) {
    return {
      networkBaseUrl: DEFAULT_BASE_URL,
    }
  };

  return {
    networkBaseUrl: searchParams.baseUrl as string ?? DEFAULT_BASE_URL,
    actionColor: searchParams?.actionColor as string,
  }
}