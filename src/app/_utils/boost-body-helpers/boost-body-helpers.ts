import { BoostEntity } from "../../_types/boost";

/**
 * Whether a given boost has a boost body.
 * @param { boolean } boost - Boost entity. 
 * @returns { boolean } - Whether the boost has a boost body.
 */
export const hasBoostBody = (boost: BoostEntity): boolean => {
  return Boolean(boost.title || boost.message || boost.link_title);
}