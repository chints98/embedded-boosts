import { BoostEntity } from "../../_types/boost";

const PROXY_BASE_URL: string = 'https://cdn.minds.com/api/v2/media/proxy?size=800&src=';

/**
 * Whether a given boost has a header image.
 * @param { boolean } boost - Boost entity. 
 * @returns { boolean } - Whether the boost has a header image.
 */
export const hasBoostHeaderImage = (boost: BoostEntity): boolean => {
  return Boolean(getRawBoostHeaderImgSrc(boost));
}

/**
 * Get boost header image source proxied through Minds CDN.
 * @param { BoostEntity } boost - Boost entity.
 * @returns { string|null } - Proxied boost header image source.
 */
export const getProxiedBoostHeaderImgSrc = (boost: BoostEntity): string|null => {
  const rawSrc: string|null = getRawBoostHeaderImgSrc(boost);

  return rawSrc ?
    PROXY_BASE_URL + encodeURIComponent(rawSrc) :
    null;
}

/**
 * Get raw boost header image source.
 * @param { BoostEntity } boost - Boost entity.
 * @returns { string|null } - Raw boost header image source.
 */
const getRawBoostHeaderImgSrc = (boost: BoostEntity): string|null => {
  return boost.thumbnails?.['large'] || boost.thumbnails?.['xlarge'] || boost.thumbnail_src || null;
}
