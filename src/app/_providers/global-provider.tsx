"use client";

import React, {PropsWithChildren, createContext, useEffect} from "react";
import { GlobalContextType } from "../_types/global-context";

/** Global context. */
export const GlobalContext = createContext<GlobalContextType>({
  networkBaseUrl: ''
});

/** Global provider props. */
type GlobalProviderProps = PropsWithChildren<{value: GlobalContextType}>;

/**
 * Global provider wrapper component.
 * @param { GlobalProviderProps } props - Global provider props.
 * @returns { JSX.Element } - Global provider component.
 */
export const GlobalProvider = ({ children, value }: GlobalProviderProps ): JSX.Element => {
  // Override action color CSS variable before storing.
  useEffect(() => {
    if (value.actionColor) {
      document.documentElement.style.setProperty('--color-action', value.actionColor);
    }
  }, [value]);

  return (
    <GlobalContext.Provider value={value}>
      {children}
    </GlobalContext.Provider>
  );
}

