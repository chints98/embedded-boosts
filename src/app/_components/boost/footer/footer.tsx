import { BoostGoalButtonText } from "../../../_enums/goalButtonText";
import { BoostEntity } from "../../../_types/boost";
import CsrOnly from "../../common/csr-only/csr-only";

/** Boost footer props. */
export type BoostFooterProps = { boost: BoostEntity };

/**
 * Boost footer component, contains core CTA.
 * @param { BoostFooterProps } props - Boost footer props.  
 * @returns { JSX.Element } - Boost footer component.
 */
export const BoostFooter = ({ boost }: BoostFooterProps): JSX.Element => {
  /**
   * Parse CTA text from goal button text enum.
   * @param { BoostGoalButtonText|null|undefined } goalButtonText - Goal button text enum.
   * @returns { string } - Parsed CTA text.
   */
  const parseCtaText = (goalButtonText: BoostGoalButtonText|null|undefined): string => {
    switch(goalButtonText) {
      case BoostGoalButtonText.SUBSCRIBE_TO_MY_CHANNEL:
        return 'Subscribe';
      case BoostGoalButtonText.GET_CONNECTED:
        return 'Get connected';
      case BoostGoalButtonText.STAY_IN_THE_LOOP:
        return 'Stay in the loop';
      case BoostGoalButtonText.GET_STARTED:
        return 'Get started';
      case BoostGoalButtonText.SIGN_UP:
        return 'Sign up';
      case BoostGoalButtonText.TRY_FOR_FREE:
        return 'Try for free';
      default:
        return 'Click here';
    }
  }

  return (
    <div className="mx-[16px] h-lg:my-[16px] my-[8px] min-h-[36px]">
      {/* Avoid flash-in on primary color injection */}
      <CsrOnly>
        <button
          className="bg-action text-primary-inverted w-full font-bold py-[6px] px-[38px] rounded-[99px] hover:brightness-90 active:brightness-[0.8]"
          data-testid="boost-footer-cta"
          >{parseCtaText(boost.goal_button_text)}</button
        >
      </CsrOnly>
    </div>
  );
}