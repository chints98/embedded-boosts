import '@testing-library/jest-dom'
import { render, screen, within } from '@testing-library/react'
import { mockBoostEntity } from '../../../../../../__mocks__/models/mock-boost-entity'
import { BoostFooter } from '../footer';
import { BoostEntity } from '../../../../_types/boost';
import { BoostGoalButtonText } from '../../../../_enums/goalButtonText';

describe('BoostFooter', () => {
  it('renders a button when no boost goal button text is set', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      goal_button_text: null
    };

    const { container } = render(<BoostFooter boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const { getByText } = within(screen.getByTestId('boost-footer-cta'));
    const buttonElement: HTMLElement = getByText('Click here');
    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement.nodeName).toBe('BUTTON');
  });

  it('renders a button when boost goal button text is SUBSCRIBE_TO_MY_CHANNEL', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      goal_button_text: BoostGoalButtonText.SUBSCRIBE_TO_MY_CHANNEL
    }

    const { container } = render(<BoostFooter boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const { getByText } = within(screen.getByTestId('boost-footer-cta'))
    const buttonElement: HTMLElement = getByText('Subscribe');
    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement.nodeName).toBe('BUTTON');
  });

  it('renders a button when boost goal button text is GET_CONNECTED', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      goal_button_text: BoostGoalButtonText.GET_CONNECTED
    };

    const { container } = render(<BoostFooter boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const { getByText } = within(screen.getByTestId('boost-footer-cta'));
    const buttonElement: HTMLElement = getByText('Get connected');
    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement.nodeName).toBe('BUTTON');
  });

  it('renders a button when boost goal button text is STAY_IN_THE_LOOP', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      goal_button_text: BoostGoalButtonText.STAY_IN_THE_LOOP
    };

    const { container } = render(<BoostFooter boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const { getByText } = within(screen.getByTestId('boost-footer-cta'));
    const buttonElement: HTMLElement = getByText('Stay in the loop');
    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement.nodeName).toBe('BUTTON');
  });

  it('renders a button when boost goal button text is GET_STARTED', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      goal_button_text: BoostGoalButtonText.GET_STARTED
    };

    const { container } = render(<BoostFooter boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const { getByText } = within(screen.getByTestId('boost-footer-cta'));
    const buttonElement: HTMLElement = getByText('Get started');
    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement.nodeName).toBe('BUTTON');
  });

  it('renders a button when boost goal button text is SIGN_UP', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      goal_button_text: BoostGoalButtonText.SIGN_UP
    };

    const { container } = render(<BoostFooter boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const { getByText } = within(screen.getByTestId('boost-footer-cta'));
    const buttonElement: HTMLElement = getByText('Sign up');
    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement.nodeName).toBe('BUTTON');
  });

  it('renders a button when boost goal button text is TRY_FOR_FREE', () => {
    const _mockBoostEntity: BoostEntity = {
      ...mockBoostEntity,
      goal_button_text: BoostGoalButtonText.TRY_FOR_FREE
    };

    const { container } = render(<BoostFooter boost={_mockBoostEntity}/>)
    expect(container).toMatchSnapshot();

    const { getByText } = within(screen.getByTestId('boost-footer-cta'));
    const buttonElement: HTMLElement = getByText('Try for free');
    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement.nodeName).toBe('BUTTON');
  });
})