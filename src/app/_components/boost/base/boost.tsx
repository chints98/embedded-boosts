import { BoostEntity } from "../../../_types/boost";
import { BoostFooter } from "../footer/footer";
import { BoostBody } from "../body/body";
import { BoostHeader } from "../header/header";
import { hasBoostHeaderImage } from "../../../_utils/boost-header-img-helpers/boost-header-img-helpers";

/** Boost props. */
export type BoostProps = {
  boost: BoostEntity;
}

/**
 * Core boost component, laying out different sections of the Boost.
 * @param { BoostProps } props - Boost props.
 * @returns { JSX.Element } - Boost component.
 */
export default function Boost({boost}: BoostProps): JSX.Element {
  return (
    <div className="flex flex-col h-full w-full">
      { hasBoostHeaderImage(boost) && 
        <BoostHeader boost={boost}/>
      }
      <BoostBody boost={boost}/>
      <BoostFooter boost={boost}></BoostFooter>
    </div>
  );
}