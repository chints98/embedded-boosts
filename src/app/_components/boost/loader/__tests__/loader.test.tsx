import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import { mockBoostEntity } from '../../../../../../__mocks__/models/mock-boost-entity'
import { BoostLoader } from '../loader';
import { fetchBoost } from '../../../../_services/boost-service';
import { Mock } from 'node:test';

jest.mock("../../../../_services/boost-service", () => {
  return {
    fetchBoost: jest.fn(() => mockBoostEntity)
  }
});

describe('BoostLoader', () => {
  beforeEach(() => {
    jest.spyOn(console, 'error').mockImplementation(() => {});
  });

  it('renders a boost', async() => {
    (fetchBoost as Mock<any>).mockImplementation(() => mockBoostEntity);
    const { container } = render(await BoostLoader())
    expect(container).toMatchSnapshot();

    expect(screen.getByTestId('boost-header-container')).toBeInTheDocument();
    expect(screen.queryByTestId('boost-loader-error')).not.toBeInTheDocument();
  });

  it('renders an error', async() => {
    (fetchBoost as Mock<any>).mockImplementation(() => null);
    const { container } = render(await BoostLoader())
    expect(container).toMatchSnapshot();

    const errorTextElement: HTMLElement = screen.getByTestId('boost-loader-error');
    expect(errorTextElement).toBeInTheDocument();
    expect(errorTextElement.innerHTML).toBe('There was an error rendering this ad unit');
    expect(screen.queryByTestId('boost-header-container')).not.toBeInTheDocument();
  });
});