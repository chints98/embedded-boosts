import { fetchBoost } from "../../../_services/boost-service";
import { BoostEntity } from "../../../_types/boost";
import { ExternalNavigationClickArea } from "../../common/external-navigation-click-area/external-navigation-click-area";
import Boost from "../base/boost";

/**
 * Boost loader component, handling Boost fetching and surrounding ad in clickable area.
 * @returns { Promise<JSX.Element> } - Boost loader component.
 */
export const BoostLoader = async(): Promise<JSX.Element> => {
  const boost: BoostEntity|null = await fetchBoost();
  
  return boost ? (
      <ExternalNavigationClickArea boost={boost}>
        <Boost boost={boost}/>
      </ExternalNavigationClickArea>
  ) : (
    <p className="text-md font-bold" data-testid="boost-loader-error">There was an error rendering this ad unit</p>
  )
}