import dynamic from 'next/dynamic';
import { PropsWithChildren, ReactNode } from 'react';

/**
 * Pass through component intended to only render on client via lazy loading.
 * @param { PropsWithChildren } props - simple children prop.
 * @returns { JSX.Element } - CsrOnly component.
 */
const CsrOnly = ({ children }: PropsWithChildren): ReactNode => {
  return children;
};

/** Lazy load pass through component to force client side rendering of children. */
export default dynamic(() => Promise.resolve(CsrOnly), { ssr: false });
