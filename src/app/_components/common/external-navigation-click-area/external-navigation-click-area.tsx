"use client";

import { PropsWithChildren, useContext } from "react";
import { BoostEntity } from "../../../_types/boost";
import { GlobalContext } from "../../../_providers/global-provider";
import { GlobalContextType } from "../../../_types/global-context";

/* Props for ExternalNavigationClickArea. */
type ExternalNavigationClickAreaProps = PropsWithChildren<{
  boost: BoostEntity;
  target?: string;
}>;

/**
 * External navigation click area component. This wrapper exists so that behaviour
 * relying on window can be rendered client-side in a pass-through component.
 * @param { ExternalNavigationClickAreaProps } props - External navigation click area props.
 * @returns { JSX.Element } - External navigation click area component.
 */
export const ExternalNavigationClickArea = ({boost, target, children}: ExternalNavigationClickAreaProps) => {  
  const globalContext: GlobalContextType = useContext(GlobalContext);

  /**
   * Handle click event by opening the URL in a new tab.
   * @returns { void }
   */
  const handleClick = (): void => {
    window.open(
      boost.goal_button_url ||
        boost.perma_url ||
        `${globalContext.networkBaseUrl}/newsfeed/${boost.guid}`,
      target ?? '_blank'
    );
  }

  return <div onClick={handleClick} className="cursor-pointer h-full w-full" data-testid="external-navigation-click-wrapper">{children}</div>;
}