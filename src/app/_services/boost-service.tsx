import { BoostEntity } from "../_types/boost";

// const BOOST_ENDPOINT = "/api/boost"; 

/**
 * Fetch boost from external server.
 * @returns { Promise<BoostEntity> } - Boost entity.
 */
export const fetchBoost = async (): Promise<BoostEntity|null> => {
    // try {
    //     const response = await apiFetch(BOOST_ENDPOINT);

    //     if (response.status !== 200) {
    //         throw new Error("Failed to fetch boost");
    //     }

    //     return response.json().entity;
    // } catch(e: unknown) {
    //     console.error(e);
    //     return null;
    // }

    // return MOCK_BOOST_VIDEO_MESSAGE_TITLE.entity;
    return null;
};