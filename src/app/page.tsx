import { BoostLoader } from './_components/boost/loader/loader';
import { GlobalProvider } from './_providers/global-provider';
import { buildGlobalContext } from './_utils/build-global-context/build-global-context';
import { ThemeProvider } from 'next-themes';
import { SearchParams } from './_types/search-params';

/** Page props. */
type PageProps = {
  params: { slug: string };
  searchParams?: SearchParams;
}

/**
 * Core page component.
 * @param { PageProps } props - Page props.
 * @returns { JSX.Element } - Page component.
 */
export default async function Home({ params, searchParams }: PageProps) {
  return (
    <GlobalProvider value={buildGlobalContext(searchParams)}>
      <ThemeProvider attribute="class" forcedTheme={searchParams?.darkMode === 'true' ? 'dark' : 'light'}>
        <main className='flex h-full w-full min-h-[200px] min-w-[200px]'>
          <BoostLoader/>
        </main>
      </ThemeProvider>
    </GlobalProvider>
  );
}