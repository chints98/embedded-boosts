import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";

/** Inter font. */
const inter = Inter({ subsets: ["latin"] });

/** Metadata for the page. */
export const metadata: Metadata = {
  title: "Minds Embedded Boost",
  description: "Embeddable ad unit for Minds Boost"
};

/** Root layout component. */
export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className + ' h-screen w-screen'}>{children}</body>
    </html>
  );
}
