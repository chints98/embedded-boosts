/** Search params type. */
export type SearchParams = { [key: string]: string | string[] | undefined } | undefined;