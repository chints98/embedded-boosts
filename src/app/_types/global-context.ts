
/** Global context type. */
export type GlobalContextType = {
  networkBaseUrl: string;
  actionColor?: string;
}