/**
 * The Minds Embedded Boosts SDK
 * @license MIT
 * @author Mark Harding & Ben Hayward
 */
let timer;
let currentScript;

(function MindsEmbeddedBoostsSdk() {
  currentScript = document.currentScript;

  if (!getBoostSlotElement()) {
    // We will try and pick up the embedded boost script during a future dom mutation
    return;
  }

  setupIframe();
})();

function setupIframe() {
  if (isIframeSetup()) return;

  const boostSlotElement = getBoostSlotElement();

  // mark as already setup
  boostSlotElement.setAttribute("data-ready", true);

  // build the embed app link
  const iframeSrc = boostSlotElement.getAttribute("data-href") ||
    currentScript.src.replace("/js/embed.min.js", "");
  const iframeHeight = boostSlotElement.getAttribute("data-height");
  const iframeWidth = boostSlotElement.getAttribute("data-width");
  const borderColor = boostSlotElement.getAttribute("data-border-color") ?? "rgba(211, 219, 277, .5)";

  // create an iframe
  const iframe = document.createElement("iframe");
  iframe.setAttribute("name", "minds-boost-slot");
  iframe.setAttribute("src", iframeSrc);
  // TODO
  iframe.setAttribute(
    "style",
    `border: none; visibility: visible; width: ${iframeWidth}; height: ${iframeHeight}; border: 1px solid ${borderColor};`
  );

  boostSlotElement.appendChild(iframe);
}

function getBoostSlotElement() {
  const elements = document.querySelectorAll(".minds-boost-slot");

  if (!elements.length) {
    return null;
  }

  return elements[0];
}

function isIframeSetup() {
  const boostSlotElement = getBoostSlotElement();

  if (!boostSlotElement) return false;

  return boostSlotElement.getAttribute("data-ready") === 'true';
}

const observer = new MutationObserver(function () {
  if (timer) clearTimeout(timer);

  timer = setTimeout(() => {
    if (isIframeSetup()) return;

    if (getBoostSlotElement()) {
      setupIframe();
    }
  }, 300); // Wait 300ms between mutatations
});

observer.observe(document.documentElement || document.body, {
  attributes: true,
  childList: true,
});