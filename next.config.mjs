/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    formats: ["image/webp"],
    remotePatterns: [
      {
        protocol: "https",
        hostname: "cdn.minds.com",
        port: "",
      },
    ],
  },
};

export default nextConfig;
